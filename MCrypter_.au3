#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=MCrypter.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseX64=n
#AutoIt3Wrapper_Res_Comment=Text/Data MCrypter_ By M4rcellxD
#AutoIt3Wrapper_Res_Description=Text/Data MCrypter_ By M4rcellxD
#AutoIt3Wrapper_Res_Fileversion=3.0.0.0
#AutoIt3Wrapper_Res_ProductName=MCrypter _
#AutoIt3Wrapper_Res_CompanyName=m4-dev
#AutoIt3Wrapper_Res_LegalCopyright=m4-dev
#AutoIt3Wrapper_Res_LegalTradeMarks=m4-dev
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Au3Stripper=y
#Au3Stripper_Parameters=/rm /so /mo /mi 25
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs
	[CWAutCompFileInfo]
	Company=M4rcellxD
	Copyright=M4rcellxD
	Description=Text/Data MCrypter_ By M4rcellxD
	Version=3.0.0.0
	ProductName=MCrypter_
	ProductVersion=3.0.0.0
#ce

#include <GDIPlus.au3>
#include <file.au3>
#include <Crypt.au3>
#include <Misc.au3>
#include <MsgBoxConstants.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <Array.au3>



Global $hHBmp_BG, $hB, $iPerc = 0, $iSleep = 30, $s = 0, $t, $m = 0


$rnd = Random(989435148989435148, 989435144478953453454165456, 0)
$srnd = $rnd * $rnd
Global $rrnd = StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd)
Global $rndv = Random(3, 9999999, 1)
$Window_Color = "0xF0F0F0"

#Region ### START Koda GUI section ### Form=
Global $Form1_1 = GUICreate("MCrypter_", 556, 496, 418, 310, BitOR($GUI_SS_DEFAULT_GUI, $WS_MAXIMIZEBOX, $WS_SIZEBOX, $WS_THICKFRAME, $WS_TABSTOP, $DS_MODALFRAME), BitOR($WS_EX_ACCEPTFILES, $WS_EX_WINDOWEDGE))
$Form1_1context = GUICtrlCreateContextMenu()
$MenuItem4 = GUICtrlCreateMenuItem("Color", $Form1_1context)
$MenuItem2 = GUICtrlCreateMenuItem("Homepage", $Form1_1context)
$MenuItem1 = GUICtrlCreateMenuItem("AboutBox", $Form1_1context)
$Edit1 = GUICtrlCreateEdit("", 8, 16, 537, 329)
GUICtrlSetState(-1, $GUI_DROPACCEPTED)
GUICtrlSetData(-1, "")
$Group1 = GUICtrlCreateGroup("Encryption", 8, 408, 537, 73)
GUICtrlSetFont(-1, 8, 800, 0, "MS Sans Serif")
GUICtrlSetState(-1, $GUI_DROPACCEPTED)
$Input1 = GUICtrlCreateInput($rrnd, 16, 448, 417, 21)
$Label1 = GUICtrlCreateLabel("Encryption Key's:", 16, 424, 81, 17)
$randomizebutton = GUICtrlCreateButton("Random", 440, 448, 97, 25)
$Input2 = GUICtrlCreateInput("SafeKey Here", 112, 424, 153, 21)
$Button4 = GUICtrlCreateButton("Export", 488, 432, 49, 17)
$Button5 = GUICtrlCreateButton("Import", 440, 432, 49, 17)
$Input3 = GUICtrlCreateInput($rndv, 352, 424, 73, 21)
$Label6 = GUICtrlCreateLabel("Drag and Drop", 448, 417, 94, 14)
$Input4 = GUICtrlCreateInput($Window_Color, 280, 424, 57, 21)
GUICtrlCreateGroup("", -99, -99, 1, 1)

$encryptgroup = GUICtrlCreateGroup("Encrypt / Drag and drop file here", 8, 352, 265, 57)
GUICtrlSetState(-1, $GUI_DROPACCEPTED)
$EncryptButton = GUICtrlCreateButton("Encrypt", 16, 368, 201, 33)
$Button6 = GUICtrlCreateButton("File", 224, 368, 41, 33)
GUICtrlCreateGroup("", -99, -99, 1, 1)

$decryptgroup = GUICtrlCreateGroup("Decrypt / Drag and drop file here", 280, 352, 265, 57)
GUICtrlSetState(-1, $GUI_DROPACCEPTED)
$DecryptButton = GUICtrlCreateButton("Decrypt", 288, 368, 201, 33)
$Button7 = GUICtrlCreateButton("File", 496, 368, 41, 33)
GUICtrlCreateGroup("", -99, -99, 1, 1)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###



#Region loading animation
Func StartAnim($animation)
	_GDIPlus_Startup()

	Global $STM_SETIMAGE = 0x0172 ;$IMAGE_BITMAP = 0

	Global $iW = 200, $iH = 200
	Local $aPos = WinGetPos($Form1_1)
	Global $hGUI = GUICreate("Loading Spinner", $iW, $iH, ($aPos[0] + 180), ($aPos[1] + 150), $WS_POPUP)
	Global $iPic = GUICtrlCreatePic("", 0, 0, $iW, $iH)
	GUICtrlSetState(-1, $GUI_DISABLE)
	GUISetState()
	GUIRegisterMsg($WM_TIMER, $animation)
	DllCall("user32.dll", "int", "SetTimer", "hwnd", $hGUI, "int", 0, "int", $iSleep, "int", 0)
	GUISetState(@SW_DISABLE, $Form1_1)
EndFunc   ;==>StartAnim
#EndRegion loading animation


While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $EncryptButton
			_encryptgui()
		Case $DecryptButton
			_decryptgui()
		Case $Button6
			_enfile()
		Case $Button7
			_defile()
		Case $randomizebutton
			rnd()
		Case $Button4
			export()
		Case $Button5
			_import()
		Case $GUI_EVENT_DROPPED
			If @GUI_DropId = $Group1 Then
				_dropimport(@GUI_DragFile)
			ElseIf @GUI_DropId = $Edit1 Then
				_readfiletobox(@GUI_DragFile)
			ElseIf @GUI_DropId = $encryptgroup Then
				_encryptdrop(@GUI_DragFile)
			ElseIf @GUI_DropId = $decryptgroup Then
				_decryptdrop(@GUI_DragFile)
			EndIf
		Case $MenuItem2
			ShellExecute("https://toolslib.net/downloads/viewdownload/110-mcrypter/")
		Case $MenuItem1
			AboutBox()
		Case $MenuItem4
			GUISetBkColor(_Change_Window_Color(), $Form1_1)
			GUICtrlSetData($Input4, $Window_Color)
	EndSwitch
WEnd


#Region DATA ----------------------------------------------------------------------------------

Func _readfiletobox($filepath)
	$MegaByte = ('1048576')
	$FileSizeInBytes = FileGetSize($filepath)
	$Equal = $FileSizeInBytes / $MegaByte
	$Round = Round($Equal, '2')

	If $Round <= 100 Then
		$readfile = FileRead($filepath)
		FileClose($filepath)
		GUICtrlSetData($Edit1, $readfile)
	Else
		MsgBox(0, "Error", $Round & "MB are too big")
		GUICtrlSetData($Edit1, "")
	EndIf
EndFunc   ;==>_readfiletobox



Func _encryptdrop($inputpath)
	$outputpath = $inputpath & ".krypt"

	Local $iFileExists = FileExists($outputpath)
	If $iFileExists Then FileMove($outputpath, $outputpath & ".bak", 1)

	_EncryptFile($inputpath, $outputpath)     ; Data that will be encrypted and written

	;MsgBox($MB_SYSTEMMODAL, "FinishBox_", 'File successfully Encrypted')
EndFunc   ;==>_encryptdrop

Func _decryptdrop($inputpath)
	$outputpath = $inputpath
	If StringInStr($outputpath, ".", 0, -1) Then $outputpath = StringMid($outputpath, 1, StringInStr($outputpath, ".", 0, -1) - 1)

	Local $iFileExists = FileExists($outputpath)
	If $iFileExists Then FileMove($outputpath, $outputpath & ".bak", 1)

	_DecryptFile($inputpath, $outputpath)     ; Data that will be encrypted and written

	;MsgBox($MB_SYSTEMMODAL, "FinishBox_", 'File Decrypted')
EndFunc   ;==>_decryptdrop


Func _enfile()


	Local $inputpath = FileOpenDialog("MCrypter_ Encrypt small files  Files", @ScriptDir, "(*.*)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
		Return
	EndIf


	$outputpath = FileSaveDialog("MCrypter_ Save Encrypted file", @ScriptDir, "Krypt Files (*.krypt)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
		Return
	EndIf

	FileDelete($outputpath)     ;Keine überschreibungen

	_EncryptFile($inputpath, $outputpath)     ; Data that will be encrypted and written

	MsgBox($MB_SYSTEMMODAL, "FinishBox_", 'File successfully Encrypted')
EndFunc   ;==>_enfile

Func _defile()


	Local $inputpath = FileOpenDialog("MCrypter_ decrypt small Files", @ScriptDir, "Krypt Files (*.krypt)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
		Return
	EndIf


	Local $outputpath = FileSaveDialog("MCrypter_ Save dencrypted file", @ScriptDir, "(*.*)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
		Return
	EndIf

	FileDelete($outputpath)     ;Keine überschreibungen

	_DecryptFile($inputpath, $outputpath)     ; Data that will be encrypted and written

	MsgBox($MB_SYSTEMMODAL, "FinishBox_", 'File Decrypted')
EndFunc   ;==>_defile



Func _encryptgui()
	GUICtrlSetData($Edit1, _Encrypt(StringToBinary(GUICtrlRead($Edit1), $SB_UTF8)))     ; Data that will be encrypted.
EndFunc   ;==>_encryptgui

Func _decryptgui()
	GUICtrlSetData($Edit1, BinaryToString(_Decrypt(GUICtrlRead($Edit1)), $SB_UTF8))     ; Data that will be decrypted.
EndFunc   ;==>_decryptgui

#EndRegion DATA ----------------------------------------------------------------------------------

_Crypt_Startup()

#Region Encrypt ------------------------------------------------------------------------------

Func _Encrypt($sData)
	GUISetState(@SW_DISABLE, $Form1_1)
	Local $sUserKey2 = GUICtrlRead($Input1)
	Local $sUserKey1 = GUICtrlRead($Input2)
	Local $sUserKey3 = GUICtrlRead($Input3)
	Local $sUserKey4 = GUICtrlRead($Input4)
	$sUserKey = $sUserKey1 & $sUserKey2 & $sUserKey3 & $sUserKey4
	;MsgBox(0, "", $sUserKey)

	Sleep(10)
	StartAnim(PlayEncryptAnim)
	Local $bEncrypted0 = _Crypt_EncryptData($sData, $sUserKey, $CALG_AES_256)
	$sData = Null
	Local $bEncrypted1 = _Crypt_EncryptData($bEncrypted0, $sUserKey, $CALG_3DES)
	$bEncrypted0 = Null
	Local $bEncrypted2 = _Crypt_EncryptData($bEncrypted1, $sUserKey, $CALG_AES_128)
	$bEncrypted1 = Null
	Local $bEncrypted3 = _Crypt_EncryptData($bEncrypted2, $sUserKey, $CALG_AES_192)
	$bEncrypted2 = Null
	Local $bEncrypted4 = _Crypt_EncryptData($bEncrypted3, $sUserKey, $CALG_3DES)
	$bEncrypted3 = Null
	Local $bEncrypted5 = _Crypt_EncryptData($bEncrypted4, $sUserKey, $CALG_DES)
	$bEncrypted4 = Null
	Local $bEncrypted6 = _Crypt_EncryptData($bEncrypted5, $sUserKey, $CALG_RC2)
	$bEncrypted5 = Null
	Local $bEncrypted7 = _Crypt_EncryptData($bEncrypted6, $sUserKey, $CALG_AES_256)
	$bEncrypted6 = Null
	Local $bEncrypted8 = _Crypt_EncryptData($bEncrypted7, $sUserKey, $CALG_RC4)
	$bEncrypted7 = Null
	Local $bEncrypted9 = _Crypt_EncryptData($bEncrypted8, $sUserKey, $CALG_AES_256)
	$bEncrypted8 = Null
	Local $bEncrypted10 = _Crypt_EncryptData($bEncrypted9, $sUserKey, $CALG_AES_256)
	$bEncrypted9 = Null
	Local $bEncrypted11 = _Crypt_EncryptData($bEncrypted10, $sUserKey, $CALG_AES_256)
	$bEncrypted10 = Null
	GUISetState(@SW_ENABLE, $Form1_1)
	StopAnim()
	Return $bEncrypted11
EndFunc   ;==>_Encrypt

#EndRegion Encrypt ------------------------------------------------------------------------------


#Region Decrypt -----------------------------------------------------------------------------

Func _Decrypt($sData)
	GUISetState(@SW_DISABLE, $Form1_1)
	Local $sUserKey2 = GUICtrlRead($Input1)
	Local $sUserKey1 = GUICtrlRead($Input2)
	Local $sUserKey3 = GUICtrlRead($Input3)
	Local $sUserKey4 = GUICtrlRead($Input4)
	$sUserKey = $sUserKey1 & $sUserKey2 & $sUserKey3 & $sUserKey4

	;MsgBox(0, "", $sUserKey)
	Sleep(10)
	StartAnim(PlayDecryptAnim)
	Local $bEncrypted0 = _Crypt_DecryptData($sData, $sUserKey, $CALG_AES_256)
	$sData = Null
	Local $bEncrypted1 = _Crypt_DecryptData($bEncrypted0, $sUserKey, $CALG_AES_256)
	$bEncrypted0 = Null
	Local $bEncrypted2 = _Crypt_DecryptData($bEncrypted1, $sUserKey, $CALG_AES_256)
	$bEncrypted1 = Null
	Local $bEncrypted3 = _Crypt_DecryptData($bEncrypted2, $sUserKey, $CALG_RC4)
	$bEncrypted2 = Null
	Local $bEncrypted4 = _Crypt_DecryptData($bEncrypted3, $sUserKey, $CALG_AES_256)
	$bEncrypted3 = Null
	Local $bEncrypted5 = _Crypt_DecryptData($bEncrypted4, $sUserKey, $CALG_RC2)
	$bEncrypted4 = Null
	Local $bEncrypted6 = _Crypt_DecryptData($bEncrypted5, $sUserKey, $CALG_DES)
	$bEncrypted5 = Null
	Local $bEncrypted7 = _Crypt_DecryptData($bEncrypted6, $sUserKey, $CALG_3DES)
	$bEncrypted6 = Null
	Local $bEncrypted8 = _Crypt_DecryptData($bEncrypted7, $sUserKey, $CALG_AES_192)
	$bEncrypted7 = Null
	Local $bEncrypted9 = _Crypt_DecryptData($bEncrypted8, $sUserKey, $CALG_AES_128)
	$bEncrypted8 = Null
	Local $bEncrypted10 = _Crypt_DecryptData($bEncrypted9, $sUserKey, $CALG_3DES)
	$bEncrypted9 = Null
	Local $bEncrypted11 = _Crypt_DecryptData($bEncrypted10, $sUserKey, $CALG_AES_256)
	$bEncrypted10 = Null
	GUISetState(@SW_ENABLE, $Form1_1)
	StopAnim()

	Return $bEncrypted11
EndFunc   ;==>_Decrypt

#EndRegion Decrypt -----------------------------------------------------------------------------


#Region Encrypt file ------------------------------------------------------------------------------

Func _EncryptFile($inputfilepath, $outputfilepath)
	Local $sUserKey2 = GUICtrlRead($Input1)
	Local $sUserKey1 = GUICtrlRead($Input2)
	Local $sUserKey3 = GUICtrlRead($Input3)
	Local $sUserKey4 = GUICtrlRead($Input4)
	$sUserKey = $sUserKey1 & $sUserKey2 & $sUserKey3 & $sUserKey4

	StartAnim(PlayEncryptAnim)
	Sleep(10)
	_Crypt_EncryptFile($inputfilepath, $outputfilepath, $sUserKey, $CALG_AES_256)
	StopAnim()
	_TrayTip("Encryption", "Encryption Has Finished!", 5)
EndFunc   ;==>_EncryptFile

#EndRegion Encrypt file ------------------------------------------------------------------------------


#Region Decrypt file -----------------------------------------------------------------------------

Func _DecryptFile($inputfilepath, $outputfilepath)
	Local $sUserKey2 = GUICtrlRead($Input1)
	Local $sUserKey1 = GUICtrlRead($Input2)
	Local $sUserKey3 = GUICtrlRead($Input3)
	Local $sUserKey4 = GUICtrlRead($Input4)
	$sUserKey = $sUserKey1 & $sUserKey2 & $sUserKey3 & $sUserKey4

	;MsgBox(0, "", $sUserKey)

	StartAnim(PlayDecryptAnim)
	Sleep(10)
	_Crypt_DecryptFile($inputfilepath, $outputfilepath, $sUserKey, $CALG_AES_256)
	StopAnim()
	_TrayTip("Decryption", "Decryption Has Finished!", 5)

EndFunc   ;==>_DecryptFile

#EndRegion Decrypt file -----------------------------------------------------------------------------



#Region RandomGenerator ---------------------------------------------

Func rnd()
	$rnd = Random(989435148989435148, 989435143453797453458964165456, 0)
	$srnd = $rnd * $rnd
	Global $rrnd = StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd) + StringToBinary($rnd)
	GUICtrlSetData($Input1, $rrnd)
	Global $rndv = Random(3, 9999999999, 1)
	GUICtrlSetData($Input3, $rndv)
EndFunc   ;==>rnd

#EndRegion RandomGenerator ---------------------------------------------


#Region Import Key -----------------------------------------------------------------------------

Func _dropimport($KeyFile)
	$Key1 = IniRead($KeyFile, "Keys", "Code", "Fail")
	$SafeKey1 = IniRead($KeyFile, "Keys", "Key", "Fail")
	$SafeKey13 = IniRead($KeyFile, "Keys", "KeyCode", "Fail")
	$Safecolor1 = IniRead($KeyFile, "Color", "ColorCode", "Fail")
	$Encrypted = _Crypt_DecryptData(BinaryToString($Key1, $SB_UTF8), 9894351434537974534589641654 * 1340, $CALG_AES_256)
	$SEncrypted = _Crypt_DecryptData($SafeKey1, 98943514345379745345239641654 * 1420, $CALG_AES_256)
	$S3Encrypted = _Crypt_DecryptData($SafeKey13, 98943514345379745345239641654 * 1220, $CALG_AES_256)
	$C1Encrypted = _Crypt_DecryptData($Safecolor1, 98943514345379745345239641654 * 1222, $CALG_AES_256)
	GUICtrlSetData($Input1, $Encrypted)
	GUICtrlSetData($Input2, BinaryToString($SEncrypted, $SB_UTF8))
	GUICtrlSetData($Input3, BinaryToString($S3Encrypted, $SB_UTF8))
	GUICtrlSetData($Input4, $C1Encrypted)
	$Window_Color = GUICtrlRead($Input4)
	GUISetBkColor($Window_Color, $Form1_1)
	FileClose($KeyFile)
EndFunc   ;==>_dropimport

Func _import()
	$KeyFile = FileOpenDialog("MCrypter_ Import", @ScriptDir, "MKrypt Files (*.mkrypt)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
	Else
		$Key1 = IniRead($KeyFile, "Keys", "Code", "Fail")
		$SafeKey1 = IniRead($KeyFile, "Keys", "Key", "Fail")
		$SafeKey13 = IniRead($KeyFile, "Keys", "KeyCode", "Fail")
		$Safecolor1 = IniRead($KeyFile, "Color", "ColorCode", "Fail")
		$Encrypted = _Crypt_DecryptData(BinaryToString($Key1, $SB_UTF8), 9894351434537974534589641654 * 1340, $CALG_AES_256)
		$SEncrypted = _Crypt_DecryptData($SafeKey1, 98943514345379745345239641654 * 1420, $CALG_AES_256)
		$S3Encrypted = _Crypt_DecryptData($SafeKey13, 98943514345379745345239641654 * 1220, $CALG_AES_256)
		$C1Encrypted = _Crypt_DecryptData($Safecolor1, 98943514345379745345239641654 * 1222, $CALG_AES_256)
		GUICtrlSetData($Input1, $Encrypted)
		GUICtrlSetData($Input2, BinaryToString($SEncrypted, $SB_UTF8))
		GUICtrlSetData($Input3, BinaryToString($S3Encrypted, $SB_UTF8))
		GUICtrlSetData($Input4, $C1Encrypted)
		$Window_Color = GUICtrlRead($Input4)
		GUISetBkColor($Window_Color, $Form1_1)
	EndIf
EndFunc   ;==>_import

#EndRegion Import Key -----------------------------------------------------------------------------


#Region Export Key -----------------------------------------------------------------------------

Func export()
	$saveKeyFile = FileSaveDialog("MCrypter_ Export", @ScriptDir, "MKrypt Files (*.mkrypt)")
	If @error Then
		;MsgBox($MB_SYSTEMMODAL, "", "No file(s) were selected.")
		FileChangeDir(@ScriptDir)
	Else
		$Key = GUICtrlRead($Input1)
		$SafeKey = GUICtrlRead($Input2)
		$SafeKey3 = GUICtrlRead($Input3)
		$Safecolor = GUICtrlRead($Input4)
		$Encrypted = _Crypt_EncryptData($Key, 9894351434537974534589641654 * 1340, $CALG_AES_256)
		$SEncrypted = _Crypt_EncryptData(StringToBinary($SafeKey, $SB_UTF8), 98943514345379745345239641654 * 1420, $CALG_AES_256)
		$S3Encrypted = _Crypt_EncryptData($SafeKey3, 98943514345379745345239641654 * 1220, $CALG_AES_256)
		$C1Encrypted = _Crypt_EncryptData($Safecolor, 98943514345379745345239641654 * 1222, $CALG_AES_256)
		;If StringInStr($saveKeyFile, ".", 0, -1) Then $saveKeyFile = StringMid($saveKeyFile, 1, StringInStr($saveKeyFile, ".", 0, -1) - 1)
		FileDelete($saveKeyFile)
		IniWrite($saveKeyFile, "Keys", "Code", StringToBinary($Encrypted, $SB_UTF8))
		IniWrite($saveKeyFile, "Keys", "Key", $SEncrypted)
		IniWrite($saveKeyFile, "Keys", "KeyCode", $S3Encrypted)
		IniWrite($saveKeyFile, "Color", "ColorCode", $C1Encrypted)
	EndIf
EndFunc   ;==>export

#EndRegion Export Key -----------------------------------------------------------------------------


#Region Change Window color -----------------------------------------------------------------

Func _Change_Window_Color()     ; Change Window Color
	$New_Window_Color = _ChooseColor(2, $Window_Color, 2)
	;MsgBox(0, "$New_Window_Color", $New_Window_Color)
	$Window_Color = $New_Window_Color
	Return $Window_Color
EndFunc   ;==>_Change_Window_Color

#EndRegion Change Window color -----------------------------------------------------------------


Func AboutBox()
	MsgBox($MB_SYSTEMMODAL, "AboutBox_", 'MCrypter_' & @LF & 'Made By M4-Dev (M4-Dev.net)' & @LF & 'Version: ' & FileGetVersion(@AutoItExe) & @LF & 'Animations by UEZ' & @LF & 'Data Encryption (Textbox) uses :' & @LF & 'AES 256, AES 128, 3DES, AES_192, DES, RC2, RC4' & @LF & 'at the same time' & @LF & 'File encryption uses:' & @LF & 'AES' & @LF & 'at the same time')
EndFunc   ;==>AboutBox


Func _TrayTip(Const ByRef $sTitle, Const ByRef $sMsg, Const ByRef $iTimeout, Const $iOption = 0)
	Local $hwnd_timer

	TrayTip($sTitle, $sMsg, $iTimeout, $iOption)

	$hwnd_timer = TimerInit()
	While (($iTimeout * 1000) - TimerDiff($hwnd_timer) > 0)
		Sleep(10)
	WEnd

	Opt("trayiconhide", 1) ;Momentarily hides AutoIt Tray Icon as TrayTip cannot function without a tray icon.
	Opt("trayiconhide", 0) ;Re-enables AutoIt Tray Icon.
EndFunc   ;==>_TrayTip


#Region loading




Func StopAnim()
	GUIRegisterMsg($WM_TIMER, "")
	_WinAPI_DeleteObject($hHBmp_BG)
	_GDIPlus_Shutdown()
	GUIDelete($hGUI)
	GUISetState(@SW_ENABLE, $Form1_1)
EndFunc   ;==>StopAnim

Func PlayDecryptAnim()
	$hHBmp_BG = _GDIPlus_MultiColorLoader($iW, $iH, "Decrypting")
	$hB = GUICtrlSendMsg($iPic, $STM_SETIMAGE, $IMAGE_BITMAP, $hHBmp_BG)
	If $hB Then _WinAPI_DeleteObject($hB)
	_WinAPI_DeleteObject($hHBmp_BG)
EndFunc   ;==>PlayDecryptAnim

Func PlayEncryptAnim()
	$hHBmp_BG = _GDIPlus_MultiColorLoader($iW, $iH, "Encrypting")
	$hB = GUICtrlSendMsg($iPic, $STM_SETIMAGE, $IMAGE_BITMAP, $hHBmp_BG)
	If $hB Then _WinAPI_DeleteObject($hB)
	_WinAPI_DeleteObject($hHBmp_BG)
EndFunc   ;==>PlayEncryptAnim


Func _GDIPlus_MultiColorLoader($iW, $iH, $sText = "LOADING", $sFont = "Verdana", $bHBitmap = True)
	Local Const $hBitmap = _GDIPlus_BitmapCreateFromScan0($iW, $iH)
	Local Const $hGfx = _GDIPlus_ImageGetGraphicsContext($hBitmap)
	_GDIPlus_GraphicsSetSmoothingMode($hGfx, 4 + (@OSBuild > 5999))
	_GDIPlus_GraphicsSetTextRenderingHint($hGfx, 3)
	_GDIPlus_GraphicsSetPixelOffsetMode($hGfx, $GDIP_PIXELOFFSETMODE_HIGHQUALITY)
	_GDIPlus_GraphicsClear($hGfx, 0xFFFFFFFF)

	Local $iRadius = ($iW > $iH) ? $iH * 0.6 : $iW * 0.6

	Local Const $hPath = _GDIPlus_PathCreate()
	_GDIPlus_PathAddEllipse($hPath, ($iW - ($iRadius + 24)) / 2, ($iH - ($iRadius + 24)) / 2, $iRadius + 24, $iRadius + 24)

	Local $hBrush = _GDIPlus_PathBrushCreateFromPath($hPath)
	_GDIPlus_PathBrushSetCenterColor($hBrush, 0xFFFFFFFF)
	_GDIPlus_PathBrushSetSurroundColor($hBrush, 0x000000)
	_GDIPlus_PathBrushSetGammaCorrection($hBrush, True)

	Local $aBlend[4][2] = [[3]]
	$aBlend[1][0] = 0 ;0% center color
	$aBlend[1][1] = 0 ;position = boundary
	$aBlend[2][0] = 0.33 ;70% center color
	$aBlend[2][1] = 0.1 ;10% of distance boundary->center point
	$aBlend[3][0] = 1 ;100% center color
	$aBlend[3][1] = 1 ;center point
	_GDIPlus_PathBrushSetBlend($hBrush, $aBlend)

	Local $aRect = _GDIPlus_PathBrushGetRect($hBrush)
	_GDIPlus_GraphicsFillRect($hGfx, $aRect[0], $aRect[1], $aRect[2], $aRect[3], $hBrush)

	_GDIPlus_PathDispose($hPath)
	_GDIPlus_BrushDispose($hBrush)

	Local Const $hBrush_Black = _GDIPlus_BrushCreateSolid(0xFFFFFFFF)
	_GDIPlus_GraphicsFillEllipse($hGfx, ($iW - ($iRadius + 10)) / 2, ($iH - ($iRadius + 10)) / 2, $iRadius + 10, $iRadius + 10, $hBrush_Black)

	Local Const $hBitmap_Gradient = _GDIPlus_BitmapCreateFromScan0($iRadius, $iRadius)
	Local Const $hGfx_Gradient = _GDIPlus_ImageGetGraphicsContext($hBitmap_Gradient)
	_GDIPlus_GraphicsSetSmoothingMode($hGfx_Gradient, 4 + (@OSBuild > 5999))
	Local Const $hMatrix = _GDIPlus_MatrixCreate()
	Local Static $r = 0
	_GDIPlus_MatrixTranslate($hMatrix, $iRadius / 2, $iRadius / 2)
	_GDIPlus_MatrixRotate($hMatrix, $r)
	_GDIPlus_MatrixTranslate($hMatrix, -$iRadius / 2, -$iRadius / 2)
	_GDIPlus_GraphicsSetTransform($hGfx_Gradient, $hMatrix)
	$r += 10
	Local Const $hBrush_Gradient = _GDIPlus_LineBrushCreate($iRadius, $iRadius / 2, $iRadius, $iRadius, 0xFFFFFFFF, 0xFF33CAFD, 1)
	_GDIPlus_LineBrushSetGammaCorrection($hBrush_Gradient)
	_GDIPlus_GraphicsFillEllipse($hGfx_Gradient, 0, 0, $iRadius, $iRadius, $hBrush_Gradient)
	_GDIPlus_GraphicsFillEllipse($hGfx_Gradient, 4, 4, $iRadius - 8, $iRadius - 8, $hBrush_Black)
	_GDIPlus_GraphicsDrawImageRect($hGfx, $hBitmap_Gradient, ($iW - $iRadius) / 2, ($iH - $iRadius) / 2, $iRadius, $iRadius)
	_GDIPlus_BrushDispose($hBrush_Gradient)
	_GDIPlus_BrushDispose($hBrush_Black)
	_GDIPlus_GraphicsDispose($hGfx_Gradient)
	_GDIPlus_BitmapDispose($hBitmap_Gradient)
	_GDIPlus_MatrixDispose($hMatrix)

	Local Const $hFormat = _GDIPlus_StringFormatCreate()
	Local Const $hFamily = _GDIPlus_FontFamilyCreate($sFont)
	Local Const $hFont = _GDIPlus_FontCreate($hFamily, $iRadius / 10)
	_GDIPlus_StringFormatSetAlign($hFormat, 1)
	_GDIPlus_StringFormatSetLineAlign($hFormat, 1)
	Local $tLayout = _GDIPlus_RectFCreate(0, 0, $iW, $iH)
	Local Static $iColor = 0x00, $iDir = 13
	Local $hBrush_txt = _GDIPlus_BrushCreateSolid(0xFF000000 + 0x010000 * $iColor + 0x0100 * $iColor + $iColor)
	_GDIPlus_GraphicsDrawStringEx($hGfx, $sText, $hFont, $tLayout, $hFormat, $hBrush_txt)
	$iColor += $iDir
	If $iColor > 0xFF Then
		$iColor = 0xFF
		$iDir *= -1
	ElseIf $iColor < 0x16 Then
		$iDir *= -1
		$iColor = 0x16
	EndIf
	_GDIPlus_BrushDispose($hBrush_txt)
	_GDIPlus_FontDispose($hFont)
	_GDIPlus_FontFamilyDispose($hFamily)
	_GDIPlus_StringFormatDispose($hFormat)
	_GDIPlus_GraphicsDispose($hGfx)

	If $bHBitmap Then
		Local $hHBITMAP = _GDIPlus_BitmapCreateHBITMAPFromBitmap($hBitmap)
		_GDIPlus_BitmapDispose($hBitmap)
		Return $hHBITMAP
	EndIf
	Return $hBitmap
EndFunc   ;==>_GDIPlus_MultiColorLoader


#EndRegion loading
